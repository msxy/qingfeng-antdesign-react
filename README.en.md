# 青锋Cloud+React前后端分离后台管理系统-SpringCloud+Alibaba+Nacas+OAuth2Jwt+Gateway+skywalking等技术

#### Description
注意：后端依附于springcloud架构，和vue-antdesign版本使用一套后台。
青锋Cloud+React前后端分离后台管理系统，后端采用：SpringCloud+Alibaba+Nacas+OAuth2Jwt+Gateway+skywalking+Feign+Spring Boot Admin等技术；前端采用React+ant design。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
