﻿export default [
  {
    path: '/',
    component: '../layouts/BlankLayout',
    routes: [
      {
        path: '/user',
        component: '../layouts/UserLayout',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './User/login',
          },
        ],
      },
      {
        path: '/',
        component: '../layouts/SecurityLayout',
        routes: [
          {
            path: '/',
            component: '../layouts/BasicLayout',
            authority: ['admin', 'user'],
            routes: [
              {
                path: '/',
                redirect: '/welcome',
              },
              {
                path: '/welcome',
                name: 'welcome',
                icon: 'smile',
                component: './Welcome',
              },
              {
                path: '/admin',
                name: 'admin',
                icon: 'crown',
                component: './Admin',
                authority: ['admin'],
                routes: [
                  {
                    path: '/admin/sub-page',
                    name: 'sub-page',
                    icon: 'smile',
                    component: './Welcome',
                    authority: ['admin'],
                  },
                ],
              },
              {
                name: 'list.table-list',
                icon: 'table',
                path: '/list',
                component: './TableList',
              },
              {
                path: '/system',
                name: '系统管理',
                icon: 'crown',
                authority: ['admin'],
                routes: [
                  {
                    path: '/system/user',
                    name: '用户管理',
                    icon: 'smile',
                    component: './system/user',
                    authority: ['admin'],
                  },{
                    path: '/system/organize',
                    name: '组织管理',
                    icon: 'smile',
                    component: './system/organize',
                    authority: ['admin'],
                  },
                  {
                    path: '/system/role',
                    name: '角色管理',
                    icon: 'smile',
                    component: './system/role',
                    authority: ['admin'],
                  },
                  {
                    path: '/system/dictionary',
                    name: '字典管理',
                    icon: 'smile',
                    component: './system/dictionary',
                    authority: ['admin'],
                  },
                  {
                    path: '/system/menu',
                    name: '菜单管理',
                    icon: 'smile',
                    component: './system/menu',
                    authority: ['admin'],
                  },
                  {
                    path: '/system/area',
                    name: '地区管理',
                    icon: 'smile',
                    component: './system/area',
                    authority: ['admin'],
                  },
                  {
                    path: '/system/group',
                    name: '用户组管理',
                    icon: 'smile',
                    component: './system/group',
                    authority: ['admin'],
                  },
                  {
                    path: '/system/Gencode',
                    name: '代码生成器',
                    icon: 'crown',
                    component: './system/Gencode',
                    authority: ['admin']
                  },
                ],
              },
              {
                path: '/quartz',
                name: 'quartz定时器',
                routes: [
                  {
                    path: '/quartz/Cron',
                    name: 'cron表达式',
                    component: './quartz/Cron',
                  },{
                    path: '/quartz/CronSelect',
                    name: 'cron表达式',
                    component: './quartz/CronSelect',
                  },{
                    path: '/quartz/TimTask',
                    name: 'quartz任务管理',
                    component: './quartz/TimTask',
                  },{
                    path: '/quartz/busTask',
                    name: '业务任务案例',
                    component: './quartz/busTask',
                  }
                ],
              },
              {
                path: '/monitor',
                name: '监控管理',
                routes: [
                  {
                    path: '/monitor/server',
                    name: '服务监控',
                    component: './monitor/server',
                  }
                ],
              },
              {
                path: '/example',
                name: '案例管理',
                icon: 'crown',
                authority: ['admin'],
                routes: [
                  {
                    path: '/example/userOrganize',
                    name: '选择用户组织',
                    icon: 'smile',
                    component: './example/userOrganize',
                    authority: ['admin']
                  },{
                    path: '/example/openHref',
                    name: '外链跳转',
                    icon: 'smile',
                    component: './example/openHref',
                    authority: ['admin']
                  },{
                    path: '/example/iframe',
                    name: 'iframe嵌套',
                    icon: 'smile',
                    component: './example/iframe',
                    authority: ['admin']
                  },{
                    path: '/example/richText',
                    name: 'iframe嵌套',
                    icon: 'smile',
                    component: './example/richText',
                    authority: ['admin']
                  },{
                    path: '/gencode/mycontent',
                    name: '测试单表',
                    icon: 'smile',
                    component: './gencode/mycontent',
                    authority: ['admin']
                  },
                ],
              },
              {
                path: '/gencode',
                name: '代码生成',
                icon: 'crown',
                authority: ['admin'],
                routes: [
                  {
                    path: '/gencode/mycontent',
                    name: '测试单表',
                    icon: 'smile',
                    component: './gencode/mycontent',
                    authority: ['admin']
                  },
                  {
                    path: '/gencode/mytree',
                    name: '测试树表',
                    icon: 'smile',
                    component: './gencode/mytree',
                    authority: ['admin']
                  },
                ],
              },
              {
                component: './404',
              },
            ],
          },
          {
            component: './404',
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
];
