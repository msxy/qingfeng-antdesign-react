import React, { useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';

const openHerf: React.FC = () => {

    useEffect(() => {
        /**
         * iframe-宽高自适应显示
         */
        const oIframe = document.getElementById("bdIframe");
        const deviceWidth = document.documentElement.clientWidth;
        const deviceHeight = document.documentElement.clientHeight;
        oIframe.style.width = Number(deviceWidth) - 220 + "px"; //数字是页面布局宽度差值
        oIframe.style.height = Number(deviceHeight) - 120 + "px"; //数字是页面布局高度差
    });

    // setInitValues(props.values);
    return (
        <PageContainer>
            <iframe
                style={{ width: '100%', border: '0px', height: '100%' }}
                // frameborder={'0'}
                scrolling="yes"
                src={'http://www.baidu.com'}
                id="bdIframe"
            ></iframe>
        </PageContainer >
    )
}
export default openHerf;
