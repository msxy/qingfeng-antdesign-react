import React, { useRef } from 'react';
 import { Editor } from '@tinymce/tinymce-react';

 export default function App() {
   const editorRef = useRef(null);
   const log = () => {
     if (editorRef.current) {
       console.log(editorRef.current.getContent());
     }
   };
   return (
     <>
       <Editor
         onInit={(evt, editor) => editorRef.current = editor}
         initialValue=""
         onEditorChange={handleEditorChange}
         apiKey='ivw6y17i9tle7joe78yt330tbpe7uygbffddq5jo578r40a0'
         init={{
           height: 500,
           menubar: false,
        //    plugins: [
        //      'advlist autolink lists link image charmap print preview anchor',
        //      'searchreplace visualblocks code fullscreen',
        //      'insertdatetime media table paste code help wordcount'
        //    ],
           toolbar: 'undo redo | formatselect | ' +
           'bold italic backcolor | alignleft aligncenter ' +
           'alignright alignjustify | bullist numlist outdent indent | ' +
           'removeformat | help',
        //    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
         }}
       />
       <button onClick={log}>Log editor content</button>
     </>
   );
 }