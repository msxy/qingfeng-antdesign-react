import React, { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';

import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import styles from './richText.less'
import '../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const openHerf: React.FC = () => {
    const [editorState, setEditorState] = useState<any>(EditorState.createEmpty());

    const onEditorStateChange = (editorState: any) => {
        setEditorState(editorState);
    }
    // setInitValues(props.values);
    return (
        <PageContainer>
            <Editor
                editorState={editorState}
                wrapperClassName="demo-wrapper"
                editorClassName={styles.demoeditor}
                onEditorStateChange={onEditorStateChange}
            />
        </PageContainer >
    )
}
export default openHerf;
