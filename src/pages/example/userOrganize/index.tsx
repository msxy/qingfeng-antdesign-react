import React, { useState, useEffect } from 'react';
import { ProFormText } from '@ant-design/pro-form';
import { PageContainer } from '@ant-design/pro-layout';
import { Form, Divider, Button } from 'antd';

import SelectOneUser from '../../system/user/components/SelectOneUser';
import SelectMoreUser from '../../system/user/components/SelectMoreUser';
import SelectOneOrganize from '../../system/organize/components/SelectOneOrganize';
import SelectMoreOrganize from '../../system/organize/components/SelectMoreOrganize';



const selectUserOrganizeForm: React.FC = () => {
    const [form] = Form.useForm();
    //单选用户
    const [oneUserModalVisible, handleOneUserModalVisible] = useState<boolean>(false);
    const [oneUserData, handleOneUserData] = useState<any>();
    //多选用户
    const [moreUserModalVisible, handleMoreUserModalVisible] = useState<boolean>(false);
    const [moreUserData, handleMoreUserData] = useState<any>();

    const [oneOrganizeModalVisible, handleOneOrganizeModalVisible] = useState<boolean>(false);
    const [oneOrganizeData, handleOneOrganizeData] = useState<any>();

    const [moreOrganizeModalVisible, handleMoreOrganizeModalVisible] = useState<boolean>(false);
    const [moreOrganizeData, handleMoreOrganizeData] = useState<any>();


    useEffect(() => {

    }, []);

    // setInitValues(props.values);
    return (
        <PageContainer>
            <Form
                form={form}
            >
                <Divider orientation="left">单选用户</Divider>
                <ProFormText
                    name="user_id"
                    width="90%"
                    placeholder="用户ID"
                />
                <ProFormText
                    name="user_name"
                    width="90%"
                    placeholder="用户名称"
                />
                <Button type="primary" onClick={() => {
                    handleOneUserModalVisible(true);
                }}>选择单用户</Button>

                <Divider orientation="left">多选用户</Divider>
                <ProFormText
                    name="user_ids"
                    width="90%"
                    placeholder="用户IDs"
                />
                <ProFormText
                    name="user_names"
                    width="90%"
                    placeholder="用户名称s"
                />
                <Button type="primary" onClick={() => {
                    handleMoreUserModalVisible(true);
                }}>选择多用户</Button>

                <Divider orientation="left">单选组织</Divider>
                <ProFormText
                    name="organize_id"
                    width="90%"
                    placeholder="组织ID"
                />
                <ProFormText
                    name="organize_name"
                    width="90%"
                    placeholder="组织名称"
                />
                <Button type="primary" onClick={() => {
                    handleOneOrganizeModalVisible(true);
                }}>选择单组织</Button>

                <Divider orientation="left">多选组织</Divider>
                <ProFormText
                    name="organize_ids"
                    width="90%"
                    placeholder="组织IDs"
                />
                <ProFormText
                    name="organize_names"
                    width="90%"
                    placeholder="组织名称s"
                />
                <Button type="primary" onClick={() => {
                    handleMoreOrganizeModalVisible(true);
                }}>选择多组织</Button>
            </Form>

            <SelectOneUser
                onSubmit={async (value: any) => {
                    let user_id = value.map((item:any) => item.id);
                    let user_name = value.map((item:any) => item.name);
                    form.setFieldsValue({
                        user_id: user_id.join(','),
                        user_name: user_name.join(',')
                      });
                    handleOneUserData(value);
                    handleOneUserModalVisible(false);
                }}
                onCancel={() => {
                    handleOneUserModalVisible(false);
                }}
                oneUserModalVisible={oneUserModalVisible}
                values={oneUserData}
            />

            <SelectMoreUser
                onSubmit={async (value: any) => {
                    let user_ids = value.map((item:any) => item.id);
                    let user_names = value.map((item:any) => item.name);
                    form.setFieldsValue({
                        user_ids: user_ids.join(','),
                        user_names: user_names.join(',')
                      });
                    handleMoreUserData(value);
                    handleMoreUserModalVisible(false);
                }}
                onCancel={() => {
                    handleMoreUserModalVisible(false);
                }}
                moreUserModalVisible={moreUserModalVisible}
                values={moreUserData}
            />

            <SelectOneOrganize
                onSubmit={async (value: any) => {
                    let organize_id = value.map((item:any) => item.id);
                    let organize_name = value.map((item:any) => item.name);
                    form.setFieldsValue({
                        organize_id: organize_id.join(','),
                        organize_name: organize_name.join(',')
                      });
                    handleOneOrganizeData(value);
                    handleOneOrganizeModalVisible(false);
                }}
                onCancel={() => {
                    handleOneOrganizeModalVisible(false);
                }}
                oneOrganizeModalVisible={oneOrganizeModalVisible}
                values={oneOrganizeData}
            />

            <SelectMoreOrganize
                onSubmit={async (value: any) => {
                    let organize_ids = value.map((item:any) => item.id);
                    let organize_names = value.map((item:any) => item.name);
                    form.setFieldsValue({
                        organize_ids: organize_ids.join(','),
                        organize_names: organize_names.join(',')
                      });
                    handleMoreOrganizeData(value);
                    handleMoreOrganizeModalVisible(false);
                }}
                onCancel={() => {
                    handleMoreOrganizeModalVisible(false);
                }}
                moreOrganizeModalVisible={moreOrganizeModalVisible}
                values={moreOrganizeData}
            />
        </PageContainer>
    )
}
export default selectUserOrganizeForm;
