export type TableListItem = {
  id: string;
  type: string;
  job_name: string;
  job_group: string;
  description: string;
  notice_user: string;
  job_class_name: string;
  cron_expression: string;
  trigger_time: string;
  trigger_state: string;
  order_by: string;
  remark: string;
  create_time: string;
  oldJobName: string;
  oldJobGroup: string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  type?: string;
  job_name?: string;
  job_group?: string;
  description?: string;
  notice_user?: string;
  job_class_name?: string;
  cron_expression?: string;
  trigger_time?: string;
  trigger_state?: string;
  order_by?: string;
  remark?: string;
  create_time?: string;
  id?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
