import React, { useState, useEffect } from 'react';
import { ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import { Form, Modal, Dropdown, Button } from 'antd';
import { useIntl, FormattedMessage } from 'umi';
import type { TableListItem } from '../data.d';
import Cron from 'react-cron-antd';

export type FormValueType = {} & Partial<TableListItem>;

export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  updateModalVisible: boolean;
  values: Partial<TableListItem>;
};

const UpdateForm: React.FC<UpdateFormProps> = (props) => {
  const [form] = Form.useForm();
  // 执行表达式
  const [value, setValue] = useState<any>();
  const [visible, setVisible] = useState<any>();
  const selectCronValue = (value: any) => {
    setValue(value)
    console.log(value)
    setVisible(false)
    form.setFieldsValue({
      cron_expression: value
    });
  }

  useEffect(() => {
    if (form && !props.updateModalVisible) {
      form.resetFields();
    }
  }, [props.updateModalVisible]);

  const intl = useIntl();
  let index = 0;
  const handleOk = () => {
    form.submit();
    if (index > 0) {
      props.onCancel()
    }
    index++;
  };
  const handleCancel = () => {
    props.onCancel()
  }
  const handleFinish = (values: Record<string, any>) => {
    props.onSubmit(values as FormValueType);
  };


  // setInitValues(props.values);
  return (
    <Modal
      width={640}
      title={intl.formatMessage({
        id: '编辑任务',
        defaultMessage: '编辑任务',
      })}
      visible={props.updateModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
    // onFinish={props.onSubmit}
    // onVisibleChange={handleOk}
    // initialValues={props.values}
    >
      <Form
        form={form}
        onFinish={handleFinish}
        initialValues={props.values}
      >
        <ProFormText
          label='任务名称'
          rules={[
            {
              required: true,
              message: '任务名称',
            },
          ]}
          width="xl"
          name="job_name"
          placeholder="请输入任务名称"
        />
        <ProFormText
          label='接收人'
          rules={[
            {
              required: true,
              message: '接收人',
            },
          ]}
          width="xl"
          name="notice_user"
          placeholder="请输入接收人"
        />
        <Dropdown
          visible={visible}
          onVisibleChange={visible => visible}
          trigger={['click']}
          placement="bottomLeft"
          overlay={<Cron onOk={selectCronValue} value={value} />}
        >
          <div>
            <div style={{ float: 'left', width: '70%' }}>
              <ProFormText
                label='执行时间表达式'
                rules={[
                  {
                    required: true,
                    message: '执行时间',
                  },
                ]}
                width="xl"
                name="cron_expression"
                placeholder="请输入执行时间"
              />
            </div>
            <div style={{ float: 'left', width: '30%', padding: '30px 0 0 10px' }}>
              <Button type="primary" onClick={(value) => {
                setVisible(true)
              }}>获取表达式</Button>
            </div>
            <div style={{ clear: 'both' }}></div>
          </div>
          {/* <Input value={value} onClick={(value) => {
              setVisible(true)
            }} /> */}
        </Dropdown>

        <ProFormTextArea width="xl" name="description" label='任务描述'
          placeholder="请输入任务描述"
        />
        <ProFormText
          label='排序'
          width="xl"
          name="order_by"
          placeholder="请输入排序"
        />
        <ProFormTextArea width="xl" name="remark" label='备注'
          placeholder="请输入备注"
        />
      </Form>
    </Modal>
  );
};

export default UpdateForm;
