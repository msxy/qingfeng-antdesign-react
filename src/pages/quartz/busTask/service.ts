import request from '@/utils/request';
import querystring from 'querystring'
import type { TableListParams, TableListItem } from './data.d';

// 查询列表
export async function queryData(params?: TableListParams) {
  let queryString = querystring.stringify(params);
  return request('/quartz/busTask/findListPage?'+queryString, {
    data: params,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

export async function addData(params: TableListItem) {
  return request('/quartz/busTask', {
    method: 'POST',
    data: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateData(params: TableListItem) {
  return request('/quartz/busTask', {
    method: 'PUT',
    data: {
      ...params,
      method: 'update',
    },
  });
}

// 删除
export async function removeData(params: { ids: string[] }) {
  return request('/quartz/busTask/'+params.ids, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

//停止恢复
export async function stopOrRestore(params:any) {
  let queryString = querystring.stringify(params);
  return request('/quartz/busTask/stopOrRestore?'+queryString, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}


//执行
export async function execution(jobName:string,jobGroup:string) {
  return request('/quartz/busTask/execution?jobName='+jobName+'&jobGroup='+jobGroup, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}



