import request from '@/utils/request';
import querystring from 'querystring'
import type { TableListParams, TableListItem } from './data.d';

// 查询列表
export async function queryData(params?: TableListParams) {
  let queryString = querystring.stringify(params);
  return request('/quartz/timTask/findListPage?'+queryString, {
    data: params,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

// 删除
export async function removeData(jobName:any,jobGroup:any) {
  return request('/quartz/timTask/del?jobName='+jobName+'&jobGroup='+jobGroup, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

// 保存或者更新
export async function saveOrUpdate(params: TableListItem) {
  return request('/quartz/timTask/saveOrUpdate', {
    method: 'POST',
    data: {
      ...params
    },
  });
}


//停止恢复
export async function stopOrRestore(jobName:string,jobGroup:string,status:string) {
  return request('/quartz/timTask/stopOrRestore?jobName='+jobName+'&jobGroup='+jobGroup+'&status='+status, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}


//执行
export async function execution(jobName:string,jobGroup:string) {
  return request('/quartz/timTask/execution?jobName='+jobName+'&jobGroup='+jobGroup, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}



