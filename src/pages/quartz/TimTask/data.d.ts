export type TableListItem = {
  id: string;
  job_name: string;
  job_group: string;
  job_class_name: string;
  cron_expression: string;
  description: string;
  trigger_state: string;
  start_time: string;
  oldJobName: string;
  oldJobGroup: string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  job_name: string;
  job_group: string;
  job_class_name: string;
  cron_expression: string;
  description: string;
  start_time: string;
  trigger_state: string;
  id?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
