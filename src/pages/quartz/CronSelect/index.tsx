import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Dropdown, Input } from 'antd';
import Cron from 'react-cron-antd';


const TableList: React.FC = (props) => {
    const [value, setValue] = useState<any>();
    const [visible, setVisible] = useState<any>();


    const onChange = (value: any) => {
        setValue(value)
        console.log(value)
        setVisible(false)
    }

    return (
        <PageContainer>
            <Dropdown
                visible={visible}
                onVisibleChange={visible=>visible}
                trigger={['click']}
                placement="bottomLeft"
                overlay={<Cron onOk={onChange} value={value} />}
            >
                <Input value={value} onClick={(value) => {
                    setVisible(true)
                }} />
            </Dropdown>
        </PageContainer>
    );
};

export default TableList;
