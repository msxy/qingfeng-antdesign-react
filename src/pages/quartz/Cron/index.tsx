import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import Cron from 'react-cron-antd';
import { Divider, Card } from 'antd';

const TableList: React.FC = () => {
    const [value, setValue] = useState<any>('* * * * * ? *');
    return (
        <PageContainer>
            <Card  style={{ backgroundColor: '#FFFFFF' }} title="Cron表达式：">{value}</Card>
            <Divider />
            <Cron value="* * * * * ? *" onOk={(value) => { setValue(value); console.log('cron:', value); }} />
        </PageContainer>
    );
};

export default TableList;
