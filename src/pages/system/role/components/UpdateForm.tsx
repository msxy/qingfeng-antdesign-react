import React, { useState ,useEffect } from 'react';
import {
  ProFormText,
  ProFormTextArea
} from '@ant-design/pro-form';
import { Form ,Modal} from 'antd';
import { useIntl, FormattedMessage } from 'umi';

import type { TableListItem } from '../data.d';

export type FormValueType = {} & Partial<TableListItem>;

export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  updateModalVisible: boolean;
  values: Partial<TableListItem>;
};

const UpdateForm: React.FC<UpdateFormProps> = (props) => {
  const [form] = Form.useForm();
  useEffect(() => {
    if (form && !props.updateModalVisible) {
      form.resetFields();
    }
  }, [props.updateModalVisible]);
  // setInitialValues(props.values)
  // useEffect(() => {
  //   console.log('---------------')
  //   setInitValues(props.values)
  //   console.log(initValues)
  // });
  const intl = useIntl();
  let index = 0;
  const handleOk = () => {
    form.submit();
    if(index>0){
      props.onCancel()
    }
    index++;
  };
  const handleCancel = () => {
    props.onCancel()
  }
  const handleFinish = (values: Record<string, any>) => {
      props.onSubmit(values as FormValueType);
  };


  // setInitValues(props.values);
  return (
    <Modal
      width={640}
      title={intl.formatMessage({
        id: '编辑角色',
        defaultMessage: '编辑角色',
      })}
      visible={props.updateModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
      // onFinish={props.onSubmit}
      // onVisibleChange={handleOk}
      // initialValues={props.values}
    >
      <Form
        form={form} 
        onFinish={handleFinish}
        initialValues={{
          id: props.values.id,
          name: props.values.name,
          short_name: props.values.short_name,
          order_by: props.values.order_by,
          remark: props.values.remark,
        }}
      >
        <ProFormText
          name="name"
          label={intl.formatMessage({
            id: '角色名称',
            defaultMessage: '角色名称',
          })}
          width="xl"
          placeholder="请输入角色名称"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入角色名称！"
                  defaultMessage="请输入角色名称！"
                />
              ),
            },
          ]}
        />
        <ProFormText
          name="short_name"
          label={intl.formatMessage({
            id: '角色简称',
            defaultMessage: '角色简称',
          })}
          width="xl"
          placeholder="请输入角色简称"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入角色简称！"
                  defaultMessage="请输入角色简称！"
                />
              ),
            },
          ]}
        />
        <ProFormText
          name="order_by"
          label={intl.formatMessage({
            id: '排序',
            defaultMessage: '排序',
          })}
          width="xl"
          placeholder="请输入排序"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入排序！"
                  defaultMessage="请输入排序！"
                />
              ),
            },
          ]}
        />
        <ProFormTextArea width="xl" name="remark" label={intl.formatMessage({
            id: '备注',
            defaultMessage: '备注',
          })}
          placeholder="请输入备注"
        />
        </Form>
    </Modal>    
  );
};

export default UpdateForm;
