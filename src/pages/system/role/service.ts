import request from '@/utils/request';
import querystring from 'querystring'
import type { TableListParams, TableListItem } from './data.d';

export async function queryData(params?: TableListParams) {
  let queryString = querystring.stringify(params);
  return request('/system/role/findListPage?'+queryString, {
    data: params,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

export async function removeData(params: { ids: string[] }) {
  return request('/system/role/'+params.ids, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

export async function addData(params: TableListItem) {
  return request('/system/role', {
    method: 'POST',
    data: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateData(params: TableListParams) {
  return request('/system/role', {
    method: 'PUT',
    data: {
      ...params,
      method: 'update',
    },
  });
}

//更新状态
export async function updateStatus(id:string,status:string) {
  return request('/system/role/updateStatus', {
    method: 'post',
    data: {
      id,
      status
    }
  });
}


//更新权限
export function updateAuth (params:any) {
  return request('/system/role/updateAuth', {
    method: 'post',
    data: {
      ...params
    }
  });
}

//获取角色菜单列表
export function findRoleMenuList (params:any) {
  return request('/system/role/findRoleMenuList', {
    data: params,
    method: 'post'
  });
}
