export type TableListItem = {
  id: string;
  name: string;
  short_name: string;
  order_by: string;
  parent_id: string;
  parent_name: string;
  remark: string;
  status: string;
  create_time: string;
  level_num: string;
  org_cascade: string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  status?: string;
  name?: string;
  short_name?: string;
  remark?: string;
  order_by?:string;
  create_time: string;
  id?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
