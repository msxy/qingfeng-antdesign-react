import request from '@/utils/request';
import querystring from 'querystring'
import type { TableListParams, TableListItem } from './data.d';

export async function queryData(params?: any) {
  let queryString = querystring.stringify(params);
  return request('/system/organize/findListPage?'+queryString, {
    data: params,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

export async function removeData(params: { ids: string[] }) {
  return request('/system/organize/'+params.ids, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

export async function addData(params: TableListItem) {
  return request('/system/organize', {
    method: 'POST',
    data: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateData(params: TableListItem) {
  return request('/system/organize', {
    method: 'PUT',
    data: {
      ...params,
      method: 'update',
    },
  });
}

//更新状态
export async function updateStatus(id:string,status:string) {
  return request('/system/organize/updateStatus', {
    method: 'post',
    data: {
      id,
      status
    }
  });
}


//获取数据列表
export function getTreeList (params:any) {
  let queryString = querystring.stringify(params);
  return request('/system/organize/getTreeList?'+queryString, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}


//获取角色权限
export function getRoleAuth (params:any) {
  return request('/system/organize/findRoleAuth', {
    data: params,
    method: 'post'
  });
}

//更新权限
export function updateAuth (params:any) {
  return request('/system/organize/updateAuth', {
    method: 'post',
    data: {
      ...params
    }
  });
}


