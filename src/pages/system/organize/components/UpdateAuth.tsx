import React, { useState ,useEffect } from 'react';
import { Modal,Transfer,message } from 'antd';
import { useIntl } from 'umi';
import type { TableListItem } from '../data.d';

export type FormValueType = {} & Partial<TableListItem>;
import { getRoleAuth } from '../service';


export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  authModalVisible: boolean;
  values: Partial<TableListItem>;
};

const UpdateAuth: React.FC<UpdateFormProps> = (props) => {

  const findRoleAuth = async (id:string) => {
    const hide = message.loading('正在查询');
    try {
      await getRoleAuth({ id: id }).then(res =>{
        let respData =  res.data;
        let mockData = [];
        let targetKeys = [];
        for (let i = 0; i < respData.roleLs.length; i++) {
          respData.roleLs[i].key = respData.roleLs[i].id
          respData.roleLs[i].title = respData.roleLs[i].name
          respData.roleLs[i].description = respData.roleLs[i].name
          mockData.push({ key: respData.roleLs[i].id, title: respData.roleLs[i].name, description: respData.roleLs[i].name })
        }
        for (let i = 0; i < respData.myRoleLs.length; i++) {
          mockData.push({ key: respData.myRoleLs[i].id, title: respData.myRoleLs[i].name, description: respData.myRoleLs[i].name })
          targetKeys.push(respData.myRoleLs[i].id)
        }
        setMockData(mockData);
        setTargetKeys(targetKeys);
      })
      hide();
      // message.success('数据查询成功');
      return true;
    } catch (error) {
      hide();
      // message.error('数据查询失败');
      return false;
    }
  };

  useEffect(() => {
    findRoleAuth(props.values.id+"");
  }, [props.authModalVisible]);


  const intl = useIntl();
  let index = 0;
  const handleOk = () => {
    let values = {};
    values = {role_ids:targetKeys.join(",")}
    props.onSubmit(values);
    if(index>0){
      props.onCancel()
    }
    index++;
  };
  const handleCancel = () => {
    props.onCancel()
  }

  const [mockData, setMockData] = useState([]);
  const [targetKeys, setTargetKeys] = useState([]);
  const [selectedKeys, setSelectedKeys] = useState([]);
  const onChange = (nextTargetKeys, direction, moveKeys) => {
    // console.log('targetKeys:', nextTargetKeys);
    // console.log('direction:', direction);
    // console.log('moveKeys:', moveKeys);
    setTargetKeys(nextTargetKeys);
  };

  const onSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
    // console.log('sourceSelectedKeys:', sourceSelectedKeys);
    // console.log('targetSelectedKeys:', targetSelectedKeys);
    setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
  };

  const onScroll = (direction, e) => {
    // console.log('direction:', direction);
    // console.log('target:', e.target);
  };
  // setInitValues(props.values);
  return (
    <Modal
      width={640}
      title={intl.formatMessage({
        id: '权限分配',
        defaultMessage: '权限分配',
      })}
      visible={props.authModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
      // onFinish={props.onSubmit}
      // onVisibleChange={handleOk}
      // initialValues={props.values}
    >
     <Transfer
      listStyle={{
        width: '600px',
        height: '300px',
        marginLeft: '20px'
      }}
      dataSource={mockData}
      titles={['角色', '授权角色']}
      targetKeys={targetKeys}
      selectedKeys={selectedKeys}
      onChange={onChange}
      onSelectChange={onSelectChange}
      onScroll={onScroll}
      render={item => item.title}
    />
    </Modal>    
  );
};

export default UpdateAuth;
