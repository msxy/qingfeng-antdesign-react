import React, { useState, useEffect, useRef } from 'react';
import { Form, Modal, Row, Col, Divider, Checkbox, Tag, Input, Button,message } from 'antd';
import { useIntl, FormattedMessage } from 'umi';
import OrganizeTree from './OrganizeTree';
import { getTreeList } from '../service';

const CheckboxGroup = Checkbox.Group;

export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: any) => void;
  onSubmit: (values: any) => Promise<void>;
  moreOrganizeModalVisible: boolean;
  values: any;
};

const SelectMoreOrganizeForm: React.FC<UpdateFormProps> = (props) => {
  const [form] = Form.useForm();
  const childref = useRef();
  //选中树形
  const [selectOrganize, setSelectOrganize] = useState<any>();
  //搜索值d
  const [searchData, setSearchData] = useState<any>();
  //查询列表
  const [organizeData, setOrganizeData] = useState<any>();
  const [organizeId, setOrganizeId] = React.useState(1);
  //选中的值
  const [value, setValue] = useState<any>([]);
  const [selectData, setSelectData] = useState<any>([]);

  useEffect(() => {
    // searchForm('1','');
    if(props.values!=''&&props.values!=null){
      setSelectData(props.values);
      let arr = props.values.map((item:any)=>item.id+":"+item.name)
      setValue(arr);
    }
  }, [props.moreOrganizeModalVisible]);


  const intl = useIntl();
  let index = 0;
  const handleOk = () => {
    props.onSubmit(selectData as any);
    if (index > 0) {
      props.onCancel()
    }
    index++;
  };
  const handleCancel = () => {
    props.onCancel()
  }

  // 处理单选框
  const onChange = (list: any) => {
    // setIndeterminate(!!list.length && list.length < plainOptions.length);
    // setCheckAll(list.length === plainOptions.length);
    setValue(list);
    let arr = list.map((item:any)=>{
      return { id: item.split(':')[0], name: item.split(':')[1] }
    })
    setSelectData(arr);
  };

  // 点击搜索按钮
  const onFinish = (values: any) => {
    setSearchData(values)
    searchForm('2', values);
  }

  //执行查询用户基本信息z
  const searchForm = async (type: string, value: any) => {
    let val = {};
    if (type == '1') {
      val = { ...searchData, parent_id: value }
    } else if (type == '2') {
      val = { ...value, parent_id: organizeId }
    }
    await getTreeList(val).then(res => {
      let respData = res.data.list;
      let options = respData.map((item: any, index: number) => {
        return { label: item.name, value: item.id + ':' + item.name };
      })
      // console.log(options)
      setOrganizeData(options);
    })
  }


  return (
    <Modal
      width={900}
      title={intl.formatMessage({
        id: '单选用户',
        defaultMessage: '单选用户',
      })}
      visible={props.moreOrganizeModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
    >
      <Form
        form={form}
        name="advanced_search"
        className="ant-advanced-search-form"
        onFinish={onFinish}
        initialValues={{
          name: '',
          short_name: ''
        }}
      >
        <Row>
          <Col span={6} style={{ padding: '0 10px' }}>
            <Form.Item
              name='name'
              label='组织名称'
            >
              <Input placeholder="组织名称" />
            </Form.Item>
          </Col>
          <Col span={6} style={{ padding: '0 10px' }}>
            <Form.Item
              name='short_name'
              label='组织简称'
            >
              <Input placeholder="组织简称" />
            </Form.Item>
          </Col>
          <Col span={6} style={{ padding: '0 10px' }}>
            <Button type="primary" htmlType="submit">
              搜索
          </Button>
            <Button
              style={{ margin: '0 8px' }}
              onClick={() => {
                form.resetFields();
              }}
            >
              清空
          </Button>
          </Col>
        </Row>
      </Form>
      <Divider orientation="left"></Divider>
      <Row>
        <Col span={8}>
          <div style={{ height: '300px', width: '280px', marginTop: '2px', overflow: 'hidden', position: 'relative' }}>
            <div style={{ overflowX: 'hidden', overflowY: 'scroll', position: 'absolute' }}>
              <div style={{ width: '280px', height: '300px' }}>
                <OrganizeTree myFef={childref} onSelect={async (value: any) => {
                  setSelectOrganize(value);
                  setOrganizeId(value.id);
                  //查询列表数据
                  searchForm('1', value.id);
                }}></OrganizeTree>
              </div>
            </div>
          </div>
        </Col>
        <Col span={16}>
          <div style={{ border: '1px solid #5FB878', height: '180px', marginTop: '2px', borderRadius: '4px', overflowY: 'scroll' }}>
            <CheckboxGroup options={organizeData} value={value} onChange={onChange} style={{ margin: '10px' }} />
          </div>
          <div id="selectValue" style={{ border: '1px solid #5FB878', height: '88px', marginTop: '2px', borderRadius: '4px', overflowY: 'scroll', padding: '4px' }}>
            {
              selectData.map((item:any) => {
                return (
                  <Tag color="#2db7f5" closable onClose={e => {
                    e.preventDefault();
                    let arr = selectData.filter((val:any)=>val.id!==item.id)
                    console.log(arr)
                    setSelectData(arr);
                  
                    let varr = value.filter((v:any)=>v.split(':')[0]!=item.id)
                    console.log(varr)
                    setValue(varr);
                  }}>
                    { item.name}
                  </Tag>
                )
              })
            }
            {/* <Tag color="#2db7f5" closable onClose={closeTag}>
              张三
            </Tag> */}
          </div>
        </Col>
      </Row >

    </Modal >
  );
};

export default SelectMoreOrganizeForm;
