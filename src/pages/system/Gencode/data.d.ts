export type TableListItem = {
  id: string;
  type: string;
  table_name: string;
  table_comment: string;
  temp_type: string;
  pack_path: string;
  mod_name: string;
  bus_name: string;
  menu_name: string;
  menu_id: string;
  gen_type: string;
  gen_path: string;
  status_type: string;
  tree_id: string;
  tree_pid: string;
  tree_name: string;
  order_by: string;
  create_time: string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  id?: string;
  type?: string;
  table_name?: string;
  table_comment?: string;
  temp_type?: string;
  pack_path?: string;
  mod_name?: string;
  bus_name?: string;
  menu_name?: string;
  menu_id?: string;
  gen_type?: string;
  gen_path?: string;
  status_type?: string;
  tree_id?: string;
  tree_pid?: string;
  tree_name?: string;
  order_by?: string;
  create_time?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
