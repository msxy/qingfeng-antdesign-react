import request from '@/utils/request';
import querystring from 'querystring'
import type { TableListParams, TableListItem } from './data.d';

/**
 * 查询table列表
 * @param params 
 * @returns 
 */
export async function getTableListPage(params: any) {
  return request('/gencode/gencode/findTableListPage', {
    method: 'POST',
    data: params,
  });
}

/**
 * 执行表的保存
 * @param params 
 * @returns 
 */
export async function saveData(params: any) {
  return request('/gencode/gencode/save', {
    method: 'POST',
    data: params,
  });
}

/**
 * 查询分页列表
 * @param params 
 * @returns 
 */
export async function queryData(params?: any) {
  let queryString = querystring.stringify(params);
  return request('/gencode/gencode/findListPage?'+queryString, {
    data: params,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

/**
 * 查询详情
 * @param id 
 * @returns 
 */
export async function findInfo(id?: any) {
  return request('/system/gencode/findInfo?id='+id, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

/**
 * 更新
 * @param params 
 * @returns 
 */
export async function updateData(params: any) {
  return request('/gencode/gencode/update', {
    method: 'POST',
    data: params,
  });
}

/**
 * 执行删除
 * @param ids 
 * @returns 
 */
export async function removeData(params: { ids: string[] }) {
  return request('/gencode/gencode/'+params.ids, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

/**
 * 代码生成gencode
 * @param id 
 * @returns 
 */
export async function createGencode(id?: any) {
  return request('/gencode/gencode/gencode?id='+id, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

/**
 * 预览代码
 * @param id 
 * @returns 
 */
export async function getViewCode(id?: any) {
  return request('/gencode/gencode/findViewCode?id='+id, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

