import React, { useState ,useEffect } from 'react';
import { Form ,Modal, Table,message} from 'antd';
import { useIntl } from 'umi';
import type { TableListItem } from '../data.d';
import { getTableListPage } from '../service';
import moment from 'moment';

export type FormValueType = {} & Partial<TableListItem>;

export type ImportFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: any) => Promise<void>;
  importModalVisible: boolean;
  values: Partial<TableListItem>;
};



const columns = [
    {
      title: '表名称',
      dataIndex: 'table_name',
    },
    {
      title: '表描述',
      dataIndex: 'table_comment',
    },
    {
      title: '创建时间',
      dataIndex: 'create_time',
      render: (dom, entity) => {
        return (
          <span>
          {moment(dom).format("YYYY-MM-DD HH:mm:ss")}
          </span>
        );
      },
    },
  ];


const ImportForm: React.FC<ImportFormProps> = (props) => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<any>([]);
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    findTableList();
  }, [props.importModalVisible]);


  const findTableList = async () => {
    const hide = message.loading('正在查询');
    try {
      await getTableListPage({}).then(res =>{
        setData(res.data)
      })
      hide();
      return true;
    } catch (error) {
      hide();
      return false;
    }
  };

  const intl = useIntl();
  const handleOk = () => {
    props.onSubmit(selectedRowKeys)
    props.onCancel()
  };
  const handleCancel = () => {
    props.onCancel()
  }

  const onSelectChange = (_,selectedRowKeys:any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    var rowKeys = selectedRowKeys.map((item:any)=>item.key)
    setSelectedRowKeys(rowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange:onSelectChange
  };




  // setInitValues(props.values);
  return (
    <Modal
      width={800}
      title={intl.formatMessage({
        id: '导入数据表',
        defaultMessage: '导入数据表',
      })}
      visible={props.importModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
      // onFinish={props.onSubmit}
      // onVisibleChange={handleOk}
      // initialValues={props.values}
    >
      <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
    </Modal>    
  );
};

export default ImportForm;
