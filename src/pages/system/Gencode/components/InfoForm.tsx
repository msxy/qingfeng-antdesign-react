import React, { useState, useEffect } from 'react';
import { Form, Modal, Card, Descriptions, Table, Select, Input, Button, Space, Switch } from 'antd';
import { useIntl } from 'umi';
import type { TableListItem } from '../data';
export type FormValueType = {} & Partial<TableListItem>;
const { Option } = Select;

const columns = [
  {
    title: '字段名称',
    dataIndex: 'field_name',
    key: 'field_name',
  },
  {
    title: '字段描述',
    dataIndex: 'field_comment',
    key: 'field_comment',
  },
  {
    title: '添加编辑',
    dataIndex: 'field_operat',
    key: 'field_operat',
    render: (text, record) => (
      <Switch defaultChecked = {text=='Y'?true:false} disabled />
    ),
  },
  {
    title: '列表展示',
    dataIndex: 'field_list',
    key: 'field_list',
    render: (text, record) => (
      <Switch defaultChecked = {text=='Y'?true:false} disabled />
    ),
  },
  {
    title: '查询展示',
    dataIndex: 'field_query',
    key: 'field_query',
    render: (text, record) => (
      <Switch defaultChecked = {text=='Y'?true:false} disabled />
    ),
  },
  {
    title: '查询方式',
    dataIndex: 'query_type',
    key: 'query_type',
    render: (text, record) => {
      var val = text;
      if(text=='='){
        val = '等于';
      }else if(text=='!='){
        val = '不等于';
      }else if(text=='>'){
        val = '大于';
      }else if(text=='>='){
        val = '大于等于';
      }else if(text=='<'){
        val = '小于';
      }else if(text=='<='){
        val = '小于等于';
      }
      return (<div>{val}</div>)
    },
  },
  {
    title: '校验规则',
    dataIndex: 'verify_rule',
    key: 'verify_rule',
    render: (text, record) => {
      var val = text;
      return (<div>{val}</div>)
    },
  },
  {
    title: '选项内容',
    dataIndex: 'option_content',
    key: 'option_content',
    render: (text, record) => {
      var val = text;
      if(text=='1'){
        val = '文本框';
      }else if(text=='2'){
        val = '文本域';
      }else if(text=='3'){
        val = '下拉框';
      }else if(text=='4'){
        val = '单选框';
      }else if(text=='5'){
        val = '复选框';
      }else if(text=='6'){
        val = '富文本';
      }else if(text=='7'){
        val = '日期控件';
      }else if(text=='8'){
        val = '上传控件';
      }
      return (<div>{val}</div>)
    },
  },
  {
    title: '默认值',
    dataIndex: 'default_value',
    key: 'default_value',
    render: (text, record) => (
      <span>{text}</span>
    ),
  },
  {
    title: '排序',
    dataIndex: 'order_by',
    key: 'order_by',
    render: (text, record) => (
      <span>{text}</span>
    ),
  },
];

export type InfoFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  infoModalVisible: boolean;
  values: Partial<TableListItem>;
};

const InfoForm: React.FC<InfoFormProps> = (props) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (form && !props.infoModalVisible) {
      form.resetFields();
    }
  }, [props.infoModalVisible]);
  
  const handleCancel = () => {
    props.onCancel()
  }

  // setInitValues(props.values);
  return (
    <Modal
      width={1000}
      title='操作'
      visible={props.infoModalVisible}
      destroyOnClose
      onCancel={handleCancel}
      footer={[
        <Button key="back" onClick={() => {
          props.onCancel()
        }}>
          关闭
        </Button>
      ]}
    >
      <Form
        form={form}
        initialValues={props.values}
        layout="vertical"
      >
        <Card title="基础信息设置" style={{ width: '100%' }}>
          <Descriptions style={{ marginBottom: 24 }} column={3}>
            <Descriptions.Item label="表名称">{props.values.table_name}</Descriptions.Item>
            <Descriptions.Item label="表描述">{props.values.table_comment}</Descriptions.Item>
            <Descriptions.Item label="模板类型">{props.values.temp_type == '0' ? '单表' : '树表'}</Descriptions.Item>
            <Descriptions.Item label="生成包路径">{props.values.pack_path}</Descriptions.Item>
            <Descriptions.Item label="生成模块名">{props.values.mod_name}</Descriptions.Item>

            <Descriptions.Item label="生成业务名">{props.values.bus_name}</Descriptions.Item>
            <Descriptions.Item label="功能名称">{props.values.menu_name}</Descriptions.Item>
            <Descriptions.Item label="上级菜单目录">{props.values.menu_name}</Descriptions.Item>
            <Descriptions.Item label="生成方式">{props.values.gen_type == '0' ? 'zip包下载' : '生成到指定路径'}</Descriptions.Item>
            <Descriptions.Item label="生成路径" style={{display:props.values.gen_type == '0'?'none':'block'}}>{props.values.gen_path}</Descriptions.Item>
          </Descriptions>
        </Card>
        <Card title="字段信息设置" style={{ width: '100%' }}>
          <Table pagination={false} columns={columns} dataSource={props.values?.fieldList} />
        </Card>
        <Card title="其他信息设置" style={{ width: '100%' }}>
          <Descriptions style={{ marginBottom: 24 }} column={1}>
            <Descriptions.Item label="排序">{props.values.order_by}</Descriptions.Item>
            <Descriptions.Item label="备注">{props.values.remark}</Descriptions.Item>
          </Descriptions>
        </Card>
      </Form>
    </Modal >
  );
};

export default InfoForm;
