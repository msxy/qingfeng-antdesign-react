import React, { useState ,useEffect } from 'react';
import {
  ProFormText,
  ProFormTextArea
} from '@ant-design/pro-form';
import { Form ,Modal} from 'antd';
import { useIntl, FormattedMessage } from 'umi';

import type { TableListItem } from '../data.d';

export type FormValueType = {} & Partial<TableListItem>;

export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  updateModalVisible: boolean;
  values: Partial<TableListItem>;
};

const UpdateForm: React.FC<UpdateFormProps> = (props) => {
  const [form] = Form.useForm();
  useEffect(() => {
    if (form && !props.updateModalVisible) {
      form.resetFields();
    }
  }, [props.updateModalVisible]);

  const intl = useIntl();
  let index = 0;
  const handleOk = () => {
    form.submit();
    if(index>0){
      props.onCancel()
    }
    index++;
  };
  const handleCancel = () => {
    props.onCancel()
  }
  const handleFinish = (values: Record<string, any>) => {
      props.onSubmit(values as FormValueType);
  };

  return (
    <Modal
      width={640}
      title={intl.formatMessage({
        id: '编辑字典',
        defaultMessage: '编辑字典',
      })}
      visible={props.updateModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
    >
      <Form
        form={form} 
        onFinish={handleFinish}
        initialValues={{
          id: props.values.id,
          name: props.values.name,
          short_name: props.values.short_name,
          code: props.values.code,
          order_by: props.values.order_by,
          remark: props.values.remark,
          parent_name: props.values.parent_name,
        }}
      >
        <ProFormText
          name="parent_name"
          label={intl.formatMessage({
            id: '父节点名称',
            defaultMessage: '父节点名称',
          })}
          width="xl"
          disabled
          placeholder="请输入父节点名称"
        />
        <ProFormText
          name="name"
          label={intl.formatMessage({
            id: '字典名称',
            defaultMessage: '字典名称',
          })}
          width="xl"
          placeholder="请输入字典名称"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入字典名称！"
                  defaultMessage="请输入字典名称！"
                />
              ),
            },
          ]}
        />
        <ProFormText
          name="short_name"
          label={intl.formatMessage({
            id: '字典简称',
            defaultMessage: '字典简称',
          })}
          width="xl"
          placeholder="请输入字典简称"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入字典简称！"
                  defaultMessage="请输入字典简称！"
                />
              ),
            },
          ]}
        />
        <ProFormText
          name="code"
          label={intl.formatMessage({
            id: '字典编码',
            defaultMessage: '字典编码',
          })}
          width="xl"
          placeholder="请输入字典编码"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入字典编码！"
                  defaultMessage="请输入字典编码！"
                />
              ),
            },
          ]}
        />
        <ProFormText
          name="order_by"
          label={intl.formatMessage({
            id: '排序',
            defaultMessage: '排序',
          })}
          width="xl"
          placeholder="请输入排序"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入排序！"
                  defaultMessage="请输入排序！"
                />
              ),
            },
          ]}
        />
        <ProFormTextArea width="xl" name="remark" label={intl.formatMessage({
            id: '备注',
            defaultMessage: '备注',
          })}
          placeholder="请输入备注"
        />
        </Form>
    </Modal>    
  );
};

export default UpdateForm;
