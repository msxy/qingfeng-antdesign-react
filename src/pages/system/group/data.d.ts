export type TableListItem = {
  id: string;
  name: string;
  short_name: string;
  user_ids: string;
  user_names: string;
  order_by: string;
  remark: string;
  status: string;
  create_time: string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  status?: string;
  name?: string;
  short_name?: string;
  user_ids: string;
  user_names: string;
  remark?: string;
  order_by?:string;
  id?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
