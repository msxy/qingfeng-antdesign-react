import React, { useState, useEffect } from 'react';
import {
  ProFormText,
  ProFormTextArea
} from '@ant-design/pro-form';
import { Form, Modal, Button } from 'antd';
import { useIntl, FormattedMessage } from 'umi';

import type { TableListItem } from '../data.d';
import SelectMoreUser from '../../../system/user/components/SelectMoreUser';

export type FormValueType = {} & Partial<TableListItem>;

export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  updateModalVisible: boolean;
  values: Partial<TableListItem>;
};

const UpdateForm: React.FC<UpdateFormProps> = (props) => {
  const [form] = Form.useForm();
  //多选用户
  const [moreUserModalVisible, handleMoreUserModalVisible] = useState<boolean>(false);
  const [moreUserData, handleMoreUserData] = useState<any>([]);

  useEffect(() => {
    if (form && !props.updateModalVisible) {
      form.resetFields();
    }
    // user_ids: props.values.user_ids,
    // user_names: props.values.user_names,
    console.log(props.values)
    if(props.values.user_ids!=''&&props.values.user_ids!=null&&props.values.user_ids!=undefined){
      let moreUser = [];
      let user_ids = props.values.user_ids?.split(',');
      console.log('user_ids:::'+user_ids)
      let user_names = props.values.user_names?.split(',');
      for(let i=0;i<user_ids.length;i++){
        moreUser.push({id:user_ids[i],name:user_names[i]});
      }
      handleMoreUserData(moreUser);
    }
  }, [props.updateModalVisible]);

  const intl = useIntl();
  let index = 0;
  const handleOk = () => {
    form.submit();
    if (index > 0) {
      props.onCancel()
    }
    index++;
  };
  const handleCancel = () => {
    props.onCancel()
  }
  const handleFinish = (values: Record<string, any>) => {
    props.onSubmit(values as FormValueType);
  };


  // setInitValues(props.values);
  return (
    <Modal
      width={640}
      title={intl.formatMessage({
        id: '编辑用户组',
        defaultMessage: '编辑用户组',
      })}
      visible={props.updateModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
    >
      <Form
        form={form}
        onFinish={handleFinish}
        initialValues={{
          id: props.values.id,
          name: props.values.name,
          short_name: props.values.short_name,
          order_by: props.values.order_by,
          remark: props.values.remark,
          user_ids: props.values.user_ids,
          user_names: props.values.user_names,
        }}
      >
        <ProFormText
          name="name"
          label={intl.formatMessage({
            id: '组名称',
            defaultMessage: '组名称',
          })}
          width="xl"
          placeholder="请输入组名称"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入组名称！"
                  defaultMessage="请输入组名称！"
                />
              ),
            },
          ]}
        />
        <ProFormText
          name="short_name"
          label={intl.formatMessage({
            id: '组简称',
            defaultMessage: '组简称',
          })}
          width="xl"
          placeholder="请输入组简称"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入组简称！"
                  defaultMessage="请输入组简称！"
                />
              ),
            },
          ]}
        />
        <ProFormText
          width="xl"
          name="user_ids"
          placeholder="请输入用户ids"
          hidden
        />
        <ProFormTextArea width="xl" label='组用户' disabled name="user_names"
          placeholder="请输入用户名称s"
        />
        <Button type="primary" size="small" onClick={() => {
          handleMoreUserModalVisible(true);
        }}>选择</Button>
        <ProFormText
          name="order_by"
          label={intl.formatMessage({
            id: '排序',
            defaultMessage: '排序',
          })}
          width="xl"
          placeholder="请输入排序"
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="请输入排序！"
                  defaultMessage="请输入排序！"
                />
              ),
            },
          ]}
        />
        <ProFormTextArea width="xl" name="remark" label={intl.formatMessage({
          id: '备注',
          defaultMessage: '备注',
        })}
          placeholder="请输入备注"
        />
      </Form>
      <SelectMoreUser
        onSubmit={async (value: any) => {
          let user_ids = value.map((item: any) => item.id);
          let user_names = value.map((item: any) => item.name);
          form.setFieldsValue({
            user_ids: user_ids.join(','),
            user_names: user_names.join(',')
          });
          handleMoreUserData(value);
          handleMoreUserModalVisible(false);
        }}
        onCancel={() => {
          handleMoreUserModalVisible(false);
        }}
        moreUserModalVisible={moreUserModalVisible}
        values={moreUserData}
      />
    </Modal>
  );
};

export default UpdateForm;
