import React, { useState ,useEffect } from 'react';
import {
  ProFormText,
  ProFormTextArea,
  ProFormSelect
} from '@ant-design/pro-form';
import { Form ,Modal} from 'antd';
import { useIntl, FormattedMessage } from 'umi';

import type { TableListItem } from '../data.d';


export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: TableListItem) => void;
  onSubmit: (values: TableListItem) => Promise<void>;
  updateModalVisible: boolean;
  values: Partial<TableListItem>;
  parentType:string;
};

const UpdateForm: React.FC<UpdateFormProps> = (props) => {
  const [form] = Form.useForm();
  //添加表单类型
  const [type, setType] = useState<any>('0');
  const [options, setOptions] = useState<any>();

  useEffect(() => {
    if (form && !props.updateModalVisible) {
      form.resetFields();
    }
    setType(props.values.type)
    form.setFieldsValue({
      type: props.values.type
    });
    if(props.values.type=='2'){
      const options = [
        { value: '2', label: '按钮', },
      ]
      setOptions(options);
    }else{
      const options = [
        { value: '0', label: '目录', },
        { value: '1', label: '菜单', },
        { value: '3', label: '外链', },
        { value: '4', label: 'iframe', },
      ]
      setOptions(options)
    }
  }, [props.updateModalVisible]);

  const intl = useIntl();
  let index = 0;
  const handleOk = () => {
    form.submit();
    if(index>0){
      props.onCancel()
    }
    index++;
  };
  const handleCancel = () => {
    props.onCancel()
  }
  const handleFinish = (values: Record<string, any>) => {
      props.onSubmit(values as TableListItem);
  };

  return (
    <Modal
      width={640}
      title={intl.formatMessage({
        id: '编辑菜单',
        defaultMessage: '编辑菜单',
      })}
      visible={props.updateModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
    >
      <Form
        form={form} 
        onFinish={handleFinish}
        initialValues={{
          id: props.values.id,
          type: props.values.type,
          name: props.values.name,
          path: props.values.path,
          redirect: props.values.redirect,
          component: props.values.component,
          title: props.values.title,
          icon: props.values.icon,
          permission: props.values.permission,
          parent_name: props.values.parent_name,
          order_by: props.values.order_by,
          remark: props.values.remark
        }}
      >
        <ProFormText
          name="parent_name"
          label={intl.formatMessage({
            id: '父节点名称',
            defaultMessage: '父节点名称',
          })}
          width="xl"
          disabled
          placeholder="请输入父节点名称"
        />
        <ProFormSelect
          options={options}
          fieldProps={{onChange:(val) => {
            setType(val);
          }}}
          // initialValue="0"
          width="md"
          name="type"
          label='选择类型'
          rules={[
            {
              required: true,
              message: '选择类型',
            },
          ]}
        />
        <ProFormText
          label='菜单名称'
          rules={[
            {
              required: true,
              message: '请输入菜单名称',
            },
          ]}
          width="xl"
          name="name"
          placeholder="请输入菜单名称"
        />
         <ProFormText
          label='路由地址'
          // rules={[
          //   {
          //     required: true,
          //     message: '请输入路由地址',
          //   },
          // ]}
          width="xl"
          name="path"
          placeholder="请输入路由地址"
          hidden={(type == '0' || type == '1'|| type == '3'|| type == '4')?false:true}
        />
         <ProFormText
          label='重定向地址'
          width="xl"
          name="redirect"
          placeholder="请输入重定向地址"
          hidden={(type == '0')?false:true}
        />
         <ProFormText
          label='组件路径'
          width="xl"
          name="component"
          hidden={type!=1?true:false}
          placeholder="请输入组件路径"
        />
         <ProFormText
          label='权限标识'
          width="xl"
          name="permission"
          placeholder="请输入权限标识"
          hidden={(type == '1' || type == '2')?false:true}
        />
         <ProFormText
          label='菜单图标'
          width="xl"
          name="icon"
          placeholder="请输入菜单图标"
          hidden={(type == '0' || type == '1'|| type == '3'|| type == '4')?false:true}
        />
        <ProFormText
          label={intl.formatMessage({
            id: '排序',
            defaultMessage: '排序',
          })}
          rules={[
            {
              required: true,
              message: '排序',
            },
          ]}
          width="xl"
          name="order_by"
          placeholder="请输入排序"
        />
        <ProFormTextArea width="xl" name="remark" label='备注'
          placeholder="请输入备注"
        />
        </Form>
    </Modal>    
  );
};

export default UpdateForm;
