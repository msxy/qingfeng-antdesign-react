import request from '@/utils/request';
import querystring from 'querystring'
import type { TableListParams, TableListItem } from './data.d';

//查询数据列表
export async function getTreeList(params?:any) {
  return request('/system/menu/findList', {
    data: params,
    method: 'post'
  });
}

// 查询分页列表
export async function queryData(params?: TableListParams) {
  let queryString = querystring.stringify(params);
  return request('/system/menu/findListPage?'+queryString, {
    data: params,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

// 删除
export async function removeData(params: { ids: string[] }) {
  return request('/system/menu/'+params.ids, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

// 添加数据
export async function addData(params: TableListItem) {
  return request('/system/menu', {
    method: 'POST',
    data: {
      ...params,
      method: 'post',
    },
  });
}

// 更新数据
export async function updateData(params: TableListItem) {
  return request('/system/menu', {
    method: 'PUT',
    data: {
      ...params,
      method: 'update',
    },
  });
}

//更新状态
export async function updateStatus(id:string,status:string) {
  return request('/system/menu/updateStatus', {
    method: 'post',
    data: {
      id,
      status
    }
  });
}
