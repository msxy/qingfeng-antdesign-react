export type TableListItem = {
  id: string;
  type: string;
  name: string;
  path: string;
  redirect: string;
  component: string;
  title: string;
  keepAlive: string;
  icon: string;
  permission: string;
  menu_cascade: string;
  parent_id: string;
  parent_name: string;
  status: string;
  level_num: string;
  order_by: string;
  remark: string;
  create_time: string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  type?: string;
  name?: string;
  path?: string;
  redirect: string;
  component?: string;
  title?:string;
  create_time: string;
  id?: string;
  keepAlive?: string;
  icon?: string;
  permission?: string;
  menu_cascade?: string;
  parent_id?: string;
  status?: string;
  level_num?: string;
  order_by?: string;
  remark?: string;
  create_time?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
