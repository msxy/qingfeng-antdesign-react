import request from '@/utils/request';
import querystring from 'querystring'
import type { TableListParams, TableListItem } from './data.d';

export async function queryData(params?: any) {
  let queryString = querystring.stringify(params);
  return request('/system/user/findListPage?'+queryString, {
    data: params,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

export async function removeData(params: { ids: string[] }) {
  return request('/system/user/'+params.ids, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

export async function addData(params: TableListItem) {
  return request('/system/user', {
    method: 'POST',
    data: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateData(params: TableListItem) {
  return request('/system/user', {
    method: 'PUT',
    data: {
      ...params,
      method: 'update',
    },
  });
}

//更新状态
export async function updateStatus(id:string,status:string) {
  return request('/system/user/updateStatus', {
    method: 'post',
    data: {
      id,
      status
    }
  });
}


//获取数据列表
export async function getList (params:any) {
  let queryString = querystring.stringify(params);
  return request('/system/user/findList?'+queryString, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

//更新密码
export function updatePwd (params:any) {
  let queryString = querystring.stringify(params);
  return request('/system/user/updatePwd?'+queryString, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}


/**
 * 获取用户组织信息分页列表
 * @param {*} params 
 * @returns 
 */
 export function getMyOrganizeListPage (params:any) {
  let queryString = querystring.stringify(params);
  return request('/system/user/findUserOrganizeListPage?'+queryString, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}


/**
 * 保存或更新用户组织信息
 * @param {*} params 
 * @returns 
 */
 export function saveOrUpdateUserOrganize (params:any) {
  return request('/system/user/saveOrUpdateUserOrganize', {
    method: 'post',
    data: params
  });
}


/**
 * 删除用户组织信息
 * @param {*} id 
 * @returns 
 */
 export function delUserOrganize(id:string) {
  return request('/system/user/delUserOrganize/'+id, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}


/**
 * 获取用户权限信息
 * @param {*} params 
 * @returns 
 */
 export function getRoleAuth (params:any) {
  return request('/system/user/findRoleAuth', {
    method: 'post',
    data: params
  });
}

/**
 * 获取组织权限信息
 * @param {*} params 
 * @returns 
 */
 export function getOrganizeAuth (params:any) {
  return request('/system/user/findOrganizeAuth', {
    method: 'post',
    data: params
  });
}

/**
 * 更新权限信息
 * @param {*} params 
 * @returns 
 */
 export function updateAuth (params:any) {
  return request('/system/user/updateAuth', {
    method: 'post',
    data: params
  });
}

/**
 * 获取用户详情
 * @param {*} id 
 * @returns 
 */
 export function getUserInfo (id) {
  return request(`/system/user/getUserInfo?id=${id}`, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}




