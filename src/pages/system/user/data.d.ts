export type TableListItem = {
  id: string;
  login_name: string;
  name: string;
  login_password: string;
  confirm_password: string;
  sex: string;
  phone: string;
  email: string;
  birth_date: string;
  motto: string;
  order_by: string;
  live_address: string;
  birth_address: string;
  organize_name: string;
  organize_id: string;
  position: string;
  remark: string;
  status: string;
  create_time: string;
  fileIds:string;
  old_login_name:string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  id?: string;
  login_name?: string;
  name?: string;
  sex?: string;
  phone?: string;
  email?: string;
  birth_date?: string;
  motto?: string;
  order_by?: string;
  live_address?: string;
  birth_address?: string;
  organize_name?: string;
  position?: string;
  live_address?: string;
  remark?: string;
  status?: string;
  create_time?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
