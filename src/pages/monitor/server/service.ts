import request from '@/utils/request';

export async function queryData(params?: any) {
  return request('/system/monitorServer/systemHardware', {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}






