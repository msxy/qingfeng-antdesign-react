import React, { useState, useEffect } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import { Row, Col, Card, Table } from 'antd';
import { queryData } from './service';

const TableList: React.FC = () => {

  const [cpuData, setCpuData] = useState<any[]>([]);
  const [ncData, setNcData] = useState<any[]>([]);
  const [fwqData, setFwqData] = useState<any[]>([]);
  const [xnjData, setXnjData] = useState<any[]>([]);
  const [cpData, setCpData] = useState<any[]>([]);

  useEffect(() => {
    findData();
  }, []);

  const findData = async () => {
    await queryData({}).then(res => {
      let data = res.data;
        //cup信息
        let cpu_data1 = {name:'核心数',value:data.cpu.cpuNum+'个'}
        let cpu_data2 = {name:'用户使用率',value:data.cpu.used+'%'}
        let cpu_data3 = {name:'系统使用率',value:data.cpu.sys+'%'}
        let cpu_data4 = {name:'当前空闲率',value:data.cpu.free+'%'}
        let cpu_data = [cpu_data1,cpu_data2,cpu_data3,cpu_data4];
        setCpuData(cpu_data);
         //cup信息
        let nc_data1 = {name:'总内存',nc_value:data.mem.total+'GB',jvm_value:data.jvm.total+'MB'}
        let nc_data2 = {name:'已用内存',nc_value:data.mem.used+'GB',jvm_value:data.jvm.used+'MB'}
        let nc_data3 = {name:'剩余内存',nc_value:data.mem.free+'GB',jvm_value:data.jvm.free+'MB'}
        let nc_data4 = {name:'使用率',nc_value:data.mem.usage+'%',jvm_value:data.jvm.usage+'%'}
        let nc_data = [nc_data1,nc_data2,nc_data3,nc_data4];
        setNcData(nc_data);
        //服务器信息
        let fwq_data1 = {computerName:data.sys.computerName,computerIp:data.sys.computerIp,osName:data.sys.osName,osArch:data.sys.osArch}
        let fwq_data = [fwq_data1];
        setFwqData(fwq_data)
        //Java虚拟机信息
        let xnj_data1 = {name1:'Java名称',value1:data.jvm.name,name2:'Java版本',value2:data.jvm.version}
        let xnj_data2 = {name1:'启动时间',value1:data.jvm.startTime,name2:'运行时长',value2:data.jvm.runTime}
        let xnj_data3 = {name1:'安装路径',value1:data.jvm.home,name2:'项目路径',value2:data.sys.userDir}
        let xnj_data = [xnj_data1,xnj_data2,xnj_data3];
        setXnjData(xnj_data);
        //磁盘信息
        setCpData(data.sysFiles);
     
    })

  }

  const cpu_columns = [
    {
      title: "属性",
      dataIndex: "name",
      key: "name",
      ellipsis: true,
    },
    {
      title: "值",
      dataIndex: "value",
      key: "value",
    },
  ];
  //内存
  const nc_columns = [
    {
      title: "属性",
      dataIndex: "name",
      key: "name",
      ellipsis: true,
    },
    {
      title: "内存",
      dataIndex: "nc_value",
      key: "nc_value",
    },
    {
      title: "JVM",
      dataIndex: "jvm_value",
      key: "jvm_value",
    },
  ];
  //服务器信息
  const fwq_columns = [
    {
      title: "服务器名称",
      dataIndex: "computerName",
      key: "computerName",
      ellipsis: true,
    },
    {
      title: "服务器IP",
      dataIndex: "computerIp",
      key: "computerIp",
    },
    {
      title: "操作系统",
      dataIndex: "osName",
      key: "osName",
    },
    {
      title: "系统架构",
      dataIndex: "osArch",
      key: "osArch",
    },
  ];
  //Java虚拟机信息
  const xnj_columns = [
    {
      title: "属性",
      dataIndex: "name1",
      key: "name1",
      ellipsis: true,
    },
    {
      title: "值",
      dataIndex: "value1",
      key: "value1",
    },
    {
      title: "属性",
      dataIndex: "name2",
      key: "name2",
      ellipsis: true,
    },
    {
      title: "值",
      dataIndex: "value2",
      key: "value2",
    },
  ];
  //磁盘状态
  const cp_columns = [
    {
      title: "盘符路径",
      dataIndex: "dirName",
      key: "dirName",
      ellipsis: true,
    },
    {
      title: "文件系统",
      dataIndex: "sysTypeName",
      key: "sysTypeName",
    },
    {
      title: "盘符类型",
      dataIndex: "typeName",
      key: "typeName",
      ellipsis: true,
    },
    {
      title: "总大小",
      dataIndex: "total",
      key: "total",
    },
    {
      title: "可用大小",
      dataIndex: "free",
      key: "free",
    },
    {
      title: "已用大小",
      dataIndex: "used",
      key: "used",
    },
    {
      title: "已用百分比",
      dataIndex: "usage",
      key: "usage",
    },
  ];


  return (
    <PageContainer>
      <Row>
        <Col span={12}>
          <Card title="CPU" style={{ width: '99%' }}>
            <Table dataSource={cpuData} columns={cpu_columns} pagination={false} />
          </Card>
        </Col>
        <Col span={12}>
          <Card title="内存" style={{ width: '99%', marginLeft: '1%' }}>
            <Table dataSource={ncData} columns={nc_columns} pagination={false} />
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Card title="服务器消息" style={{ width: '100%', marginTop: '10px' }}>
            <Table dataSource={fwqData} columns={fwq_columns} pagination={false} />
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Card title="Java虚拟机信息" style={{ width: '100%', marginTop: '10px' }}>
            <Table dataSource={xnjData} columns={xnj_columns} pagination={false} />
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Card title="磁盘状态" style={{ width: '100%', marginTop: '10px' }}>
            <Table dataSource={cpData} columns={cp_columns} pagination={false} />
          </Card>
        </Col>
      </Row>
    </PageContainer>
  );
};

export default TableList;