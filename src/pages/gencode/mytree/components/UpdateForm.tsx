import React, { useState ,useEffect ,useRef } from 'react';
import {
ModalForm, ProFormText, ProFormTextArea,ProFormSelect,ProFormRadio,ProFormCheckbox,ProFormDatePicker } from '@ant-design/pro-form';
import { Editor } from '@tinymce/tinymce-react';
import { UploadOutlined,FileOutlined,DeleteOutlined } from '@ant-design/icons';
import { Form ,Modal,Button,Upload,message} from 'antd';
import { useIntl, FormattedMessage } from 'umi';

import type { TableListItem } from '../data.d';
import { upload } from '../../../common/upload/service';

export type FormValueType = {} & Partial<TableListItem>;

export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  updateModalVisible: boolean;
  values: Partial<TableListItem>;
};

const UpdateForm: React.FC<UpdateFormProps> = (props) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (form && !props.updateModalVisible) {
      form.resetFields();
    }
  }, [props.updateModalVisible]);

  const intl = useIntl();
  let index = 0;
  const handleOk = () => {
    form.submit();
    if(index>0){
      props.onCancel()
    }
    index++;
  };
  const handleCancel = () => {
    props.onCancel()
  }
  const handleFinish = (values: Record<string, any>) => {
      props.onSubmit(values as FormValueType);
  };


  return (
    <Modal
      width={640}
      title={intl.formatMessage({
        id: '编辑',
        defaultMessage: '编辑',
      })}
      visible={props.updateModalVisible}
      destroyOnClose
      onOk={handleOk} onCancel={handleCancel}
    >
      <Form
        form={form} 
        onFinish={handleFinish}
        initialValues={props.values}
      >
          <ProFormText
                  label={intl.formatMessage({
                  id: '父节点名称',
          defaultMessage: '父节点名称',
          })}
          disabled
          width="xl"
          name="parent_name"
          placeholder="请输入父节点名称"
          />

          <ProFormText
            label='字典名称'
            rules={[
              {
                required: true,
                message: '字典名称',
              },
            ]}
            width="xl"
            name="name"
            placeholder="请输入字典名称"
          />

          <ProFormText
            label='字典简称'
            width="xl"
            name="short_name"
            placeholder="请输入字典简称"
          />

          <ProFormText
            label='排序'
            width="xl"
            name="order_by"
            placeholder="请输入排序"
          />

          <ProFormText
            label='备注'
            width="xl"
            name="remark"
            placeholder="请输入备注"
          />

        </Form>
    </Modal>    
  );
};

export default UpdateForm;
