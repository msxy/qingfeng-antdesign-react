export type TableListItem = {
  id: string;
  type: string;
  title: string;
  intro: string;
  content: string;
  read_num: string;
  status: string;
  order_by: string;
  remark: string;
  create_time: string;
  create_user: string;
  create_organize: string;
  update_user: string;
  update_time: string;
};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
    id?: string;
    type?: string;
    title?: string;
    intro?: string;
    content?: string;
    read_num?: string;
    status?: string;
    order_by?: string;
    remark?: string;
    create_time?: string;
    create_user?: string;
    create_organize?: string;
    update_user?: string;
    update_time?: string;
  id?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};