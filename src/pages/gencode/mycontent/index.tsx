import { PlusOutlined,FileOutlined,DeleteOutlined } from '@ant-design/icons';
import { Button, message, Drawer, Modal, FormInstance,Upload,Form } from 'antd';
import React, { useState, useRef, useEffect } from 'react';
import { UploadOutlined } from '@ant-design/icons';
import { Editor } from '@tinymce/tinymce-react';
import { useIntl, FormattedMessage, connect, ConnectProps } from 'umi';
import type { ConnectState } from '@/models/connect';
import type { CurrentUser } from '@/models/user';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { ModalForm, ProFormText, ProFormTextArea,ProFormSelect,ProFormRadio,ProFormCheckbox,ProFormDatePicker } from '@ant-design/pro-form';
import type { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import ProDescriptions from '@ant-design/pro-descriptions';
import type { FormValueType } from './components/UpdateForm';
import UpdateForm from './components/UpdateForm';
import { upload } from '../../common/upload/service';
  import { findDictionaryList } from '../../system/dictionary/service';
import type { TableListItem } from './data.d';
import { queryData, updateData, addData, removeData, updateStatus } from './service';

/**
 * 添加节点
 *
 * @param fields
 */
const handleAdd = async (fields: TableListItem) => {
  const hide = message.loading('正在添加');
  try {
    await addData({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};

/**
 * 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: FormValueType) => {
  const hide = message.loading('正在配置');
  try {
    await updateData(fields);
    hide();
    message.success('配置成功');
    return true;
  } catch (error) {
    hide();
    message.error('配置失败请重试！');
    return false;
  }
};

/**
 * 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: TableListItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;
  try {
    await removeData({
      ids: selectedRows.map((row) => row.id),
    });
    hide();
    message.success('删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
};


const handleRemoveOne = async (selectedRow: TableListItem) => {
  const hide = message.loading('正在删除');
  if (!selectedRow) return true;
  try {
    let params = [selectedRow.id]
    await removeData({
      ids: params,
    });
    hide();

    message.success('删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
};


const handleUpdateStatus = async (id: string, status: string) => {
  const hide = message.loading('正在更新状态');
  try {
    await updateStatus(id, status);
    message.success('状态更新成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('状态更新失败，请重试');
    return false;
  }
};

export type GlobalTableProps = {
  currentUser?: CurrentUser;
} & Partial<ConnectProps>;

const TableList: React.FC<GlobalTableProps> = (props) => {
  const formTableRef = useRef<FormInstance>();
  const formRef = useRef<FormInstance>();
  const childref = useRef();

  /** 新建窗口的弹窗 */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /** 分布更新窗口的弹窗 */
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);

  const [showDetail, setShowDetail] = useState<boolean>(false);

  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<TableListItem>();
  const [selectedRowsState, setSelectedRows] = useState<TableListItem[]>([]);

  /** 国际化配置 */
  const intl = useIntl();
  const { currentUser } = props;
  const [introFiles, setintroFiles] = useState<any>([]);
  const [contentData, setcontentData] = useState<any>([]);
  const [read_numData, setread_numData] = useState<any>([]);

  useEffect(() => {
    findDictionaryList({parent_code:'fl1001'}).then((response) => {
      let data = [];
      for(var i=0;i<response.data?.length||0;i++){
        data.push({label:response.data[i].name,value:response.data[i].id});
      }
      setcontentData(data);
    })
          const options = [
            '北京',
            '上海',
            '深圳',
          ];
          setread_numData(options);
  }, []);

  const columns: ProColumns<TableListItem>[] = [
        {
          title: '标题',
          dataIndex: 'title',
          tip: '标题',
          render: (dom, entity) => {
            return (
              <a onClick={() => {
                setCurrentRow(entity);
                setShowDetail(true);
                }}
                >
                {dom}
              </a>
            );
          },
        },
        {
          title: '标题',
          dataIndex: 'title',
          valueType: 'textarea',
        },
        {
          title: '阅读数量',
          dataIndex: 'read_num',
          valueType: 'textarea',
          hideInSearch: true,
        },
        {
          title: '排序',
          dataIndex: 'order_by',
          valueType: 'textarea',
          hideInSearch: true,
        },
    {
      title: <FormattedMessage id="创建时间" defaultMessage="创建时间" />,
      dataIndex: 'create_time',
      valueType: 'textarea',
      hideInSearch: true,
    },
    {
      title: <FormattedMessage id="pages.searchTable.titleOption" defaultMessage="操作" />,
      dataIndex: 'option',
      width: '220px',
      valueType: 'option',
      render: (_, record) => [
        <Button type="primary" size='small' key="config"
          style={{ display: currentUser?.authButton.includes('mycontent:edit') ? 'block' : 'none' }}
          onClick={() => {
            handleUpdateModalVisible(true);
            setCurrentRow(record);
          }}>
          编辑
       </Button>,
        <Button type="primary" size='small' danger
          style={{ display: currentUser?.authButton.includes('mycontent:del') ? 'block' : 'none' }}
          onClick={async () => {
            Modal.confirm({
              title: '删除任务',
              content: '确定删除该任务吗？',
              okText: '确认',
              cancelText: '取消',
              onOk: () => {
                const success = handleRemoveOne(record);
                if (success) {
                  if (actionRef.current) {
                    actionRef.current.reload();
                  }
                }
              },
            });
          }}>
          删除
      </Button>,
        <Button type="primary" size='small' style={{ display: (record.status == '0' && currentUser?.authButton.includes('mycontent:status')) ? 'block' : 'none' }} danger onClick={() => {
          const success = handleUpdateStatus(record.id, '1');
          if (success) {
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}>
          禁用
    </Button>,
        <Button type="primary" size='small' style={{ display: (record.status == '1' && currentUser?.authButton.includes('mycontent:status')) ? 'block' : 'none' }} onClick={() => {
          const success = handleUpdateStatus(record.id, '0');
          if (success) {
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}>
          启用
  </Button>,
      ],
    },
  ];

    const uploadintroFile = async (file: any) => {
      const formData = new FormData();
      formData.append("file", file.file);
      await upload(formData).then(res => {
        console.log('上传完毕11---');
        console.log(res);
        console.log(introFiles.concat(res.data))
        setintroFiles(introFiles.concat(res.data));
      })
    }
    const handleintroUploadChange = async (info: any) => {
      if (info.file.status === 'uploading') {
        return;
      }
      if (info.file.status === 'done') {
        console.log('上传完毕---');
        console.log(info);
      }
    }

    const removeintroFile = async (item: any) => {
      let data = introFiles.filter((ele:any) => ele.id !== item.id)
      setintroFiles(data);
      message.success('删除成功');
      console.log(data)
    }

  return (
    <PageContainer>
        <ProTable<TableListItem>
          headerTitle={intl.formatMessage({
            id: 'pages.searchTable.title',
            defaultMessage: '测试单表/主子表',
          })}
          actionRef={actionRef}
          formRef={formTableRef}
          rowKey="id"
          search={{
            labelWidth: 120,
          }}
          toolBarRender={() => [
            <Button
              type="primary"
              key="primary"
              style={{ display: currentUser?.authButton.includes('mycontent:add') ? 'block' : 'none' }}
              onClick={() => {
                  handleModalVisible(true);
                 setintroFiles([]);
              }}
            >
              <PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="新建" />
            </Button>,
          ]}
          request={
            (params, sorter, filter) => queryData({ ...params }).then(res => {
              const result = {
                data: res.data.rows,
                total: res.data.total,
                success: true
              }
              return result
            })
          }
          columns={columns}
          rowSelection={{
            onChange: (_, selectedRows) => {
              setSelectedRows(selectedRows);
            },
          }}
        />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            </div>
          }
        >
          <Button
            style={{ display: currentUser?.authButton.includes('mycontent:del') ? 'block' : 'none' }}
            onClick={async () => {
              Modal.confirm({
                title: '删除任务',
                content: '确定删除该任务吗？',
                okText: '确认',
                cancelText: '取消',
                onOk: () => {
                  const success = handleRemove(selectedRowsState);
                  if (success) {
                    setSelectedRows([]);
                    actionRef.current?.reloadAndRest?.();
                  }
                },
              });
            }}
          >
            <FormattedMessage id="pages.searchTable.batchDeletion" defaultMessage="批量删除" />
          </Button>
          {/* <Button type="primary">
            <FormattedMessage id="pages.searchTable.batchApproval" defaultMessage="批量审批" />
          </Button> */}
        </FooterToolbar>
      )}
      <ModalForm
        title={intl.formatMessage({
          id: '新建',
          defaultMessage: '新建',
        })}
        width="600px"
        formRef={formRef}
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          value.intro = introFiles.map((item:any) => item.id).join(',')
          const success = await handleAdd({ ...value } as TableListItem);
          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
            formRef.current?.resetFields();
          }
        }}
      >
          <ProFormTextArea width="xl"
            rules={[
              {
                required: true,
                message: '标题',
              },
            ]}
            name="title"
            label='标题'
            placeholder="请输入标题" />


            <Form.Item label="附件上传" name="requiredMarkValue">
              <Upload
                name="file"
                showUploadList={false}
                customRequest={uploadintroFile}
                onChange={handleintroUploadChange}
              >
                <Button><UploadOutlined /> 上传 </Button>
              </Upload>
              <div>
                {
                introFiles.map((item: any) => {
                  return <div id={'div_'+item.id} style={{ lineHeight: '30px' }}>
                    <a onClick={() => {
                    window.location.href =
                    "../api/system/upload/downloadFile?name=" +
                    item.name +
                    "&file_path=" +
                    item.file_path;
                    }}><FileOutlined />{item.name}</a>
                    <a onClick={() => {
                      removeintroFile(item)
                    }} style={{ paddingLeft: '10px', color: 'red' }}><DeleteOutlined /></a>
                  </div>
                })
              }
              </div>
            </Form.Item>

          <ProFormSelect width="xl"
            options={contentData}
            name="content"
            label='内容'
            placeholder="请选择内容" />

          <ProFormRadio.Group width="xl"
            options={read_numData}
            name="read_num"
            label='阅读数量'
            placeholder="请选择阅读数量" />

          <ProFormDatePicker width="xl"
            name="order_by"
            label='排序'
            placeholder="请输入排序" />
          <ProFormTextArea width="xl"
            name="remark"
            label='备注'
            placeholder="请输入备注" />

      </ModalForm>
      <UpdateForm
        onSubmit={async (value) => {
          const success = await handleUpdate({ ...value, id: currentRow?.id });
          // const success = await handleUpdate({...value,id:currentRow?.id});
          if (success) {
            handleUpdateModalVisible(false);
            setCurrentRow(undefined);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
        onCancel={() => {
          handleUpdateModalVisible(false);
          setCurrentRow(undefined);
        }}
        updateModalVisible={updateModalVisible}
        values={currentRow || {}}
      />
      <Drawer
        width={600}
        visible={showDetail}
        onClose={() => {
          setCurrentRow(undefined);
          setShowDetail(false);
        }}
        closable={false}
      >
        {currentRow?.id && (
          <ProDescriptions<TableListItem>
            column={2}
            title='详情'
            request={async () => ({
              data: currentRow || {},
            })}
            params={{
              id: currentRow?.id,
            }}
            columns={columns as ProDescriptionsItemProps<TableListItem>[]}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

// export default TableList;
export default connect(({ user, loading }: ConnectState) => ({
  currentUser: user.currentUser,
}))(TableList);