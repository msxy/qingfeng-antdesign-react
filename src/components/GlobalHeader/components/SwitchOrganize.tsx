import React, { useState, useEffect } from 'react';
import { Form, Modal, Tag, Divider, Radio, message } from 'antd';
import { getLoginUser } from './service';

export type FormValueType = {} & Partial<any>;

export type UpdateFormProps = {
    onCancel: (flag?: boolean, formVals?: FormValueType) => void;
    onSubmit: (values: FormValueType) => Promise<void>;
    switchOrganizeModalVisible: boolean;
    values: Partial<any>;
};

const UpdateForm: React.FC<UpdateFormProps> = (props) => {
    const [value, setValue] = useState<string>();
    const [options, setOptions] = useState<string>();

    const [form] = Form.useForm();
    useEffect(() => {
        findOrganize();
        setValue(props.values.orgPd.organize_id);
    },[props.switchOrganizeModalVisible]);

    const findOrganize = async () => {
        const hide = message.loading('正在查询');
        await getLoginUser().then(res => {
            let data = res.data?.organizeList;
            let options = data.map((item:any)=>{
                return { label: item.organize_name, value: item.organize_id }
            })
            setOptions(options);
        })
        hide();
    }

    const handleOk = () => {
        form.submit();
        props.onCancel()
    };
    const handleCancel = () => {
        props.onCancel()
    }
    const handleFinish = (values: Record<string, any>) => {
        values = { organize_id: value };
        props.onSubmit(values as FormValueType);
    };

    const onChangeOrganize = (e: any) => {
        setValue(e.target.value);
    };

    return (
        <Modal
            width={640}
            title='切换组织'
            visible={props.switchOrganizeModalVisible}
            destroyOnClose
            onOk={handleOk} onCancel={handleCancel}
        >
            <Form
                form={form}
                onFinish={handleFinish}
                initialValues={{
                }}
            >
                <Tag color="#2db7f5">{props.values.orgPd.organize_name}</Tag>
                <Divider />
                <Radio.Group options={options} onChange={onChangeOrganize} value={value} />
            </Form>
        </Modal>
    );
};

export default UpdateForm;
