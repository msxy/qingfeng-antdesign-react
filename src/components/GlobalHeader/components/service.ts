import request from '@/utils/request';
import querystring from 'querystring'

export async function updateData(params: any) {
  return request('/system/user', {
    method: 'PUT',
    data: {
      ...params,
      method: 'update',
    },
  });
}

//更新密码
export function updatePwd (params:any) {
  let queryString = querystring.stringify(params);
  return request('/system/user/updatePwd?'+queryString, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}

//更新切换的组织
export function updateSwitchOrganize (params:any) {
  return request('/system/user/updateSwitchOrganize', {
    method: 'post',
    data: params
  });
}

export function getLoginUser () {
  return request('/system/user/findLoginUser', {
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });
}




