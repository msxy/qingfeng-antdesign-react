/** Request 网络请求工具 更详细的 api 文档: https://github.com/umijs/umi-request */
import { extend } from 'umi-request';
import { notification } from 'antd';
import { refresh } from '@/services/auth/login';

const codeMessage: { [status: number]: string } = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

// 请求超时时间，10s
const requestTimeOut = 10 * 1000
// 更换令牌的时间区间
const checkRegion = 5 * 60 * 1000

/** 异常处理程序 */
const errorHandler = (error: { response: Response }): Response => {
  const { response } = error;
  if (response && response.status) {
    const errorText = codeMessage[response.status] || response.statusText;
    const { status, url } = response;

    notification.error({
      message: `请求错误 ${status}: ${url}`,
      description: errorText,
    });
  } else if (!response) {
    notification.error({
      description: '您的网络发生异常，无法连接服务器',
      message: '网络异常',
    });
  }
  return response;
};

/** 配置request请求时的默认参数 */
const request = extend({
  prefix: "/api",
  // baseURL: process.env.VUE_APP_API_BASE_URL,
  errorHandler, // 默认错误处理
  credentials: 'include', // 默认请求是否带上cookie
  timeout: requestTimeOut,
  responseType: 'json',
});


request.interceptors.request.use(async (url, options) => {
  console.log('-------------------------')
  console.log(url);
  console.log(options);
  // return (
  //   {
  //     url: url,
  //     options: { ...options},
  //   }
  // );
  const headers = options.headers;
  if(headers['Authorization']==''||headers['Authorization']==null){
    console.log('--------------进来了---------------')
    const expireTime = localStorage.getItem("expireTime")
    console.log(expireTime)
    if (expireTime) {
      const left = Number(expireTime) - new Date().getTime()
      const refreshToken = localStorage.getItem("refresh_token")
      if (left < checkRegion && refreshToken) {
        if (left < 0) {
          localStorage.removeItem("expireTime");
          localStorage.removeItem("refresh_token");
          localStorage.removeItem("access_token");
        }
        let accessToken = queryRefreshToken(refreshToken);
        headers['Authorization'] = 'bezarer ' + accessToken;
      } else {
        const accessToken = localStorage.getItem("access_token");
        if (accessToken) {
          headers['Authorization'] = 'bearer ' + accessToken;
        }
      }
    }
  }
  console.log('-----------------111-------------')
  console.log(headers)
  return (
    {
      url: url,
      options: { ...options, headers: headers },
    }
  );
  
})


async function queryRefreshToken(refresh_token:string) {
  let param = '';
  refresh(refresh_token).then(result => {
    if (result.status === 'success') {
      saveData(result.data)
      param = localStorage.get("access_token");
    }
  })
  return param
}


function saveData(data:any) {
  localStorage.setItem("access_token",data.access_token);
  localStorage.setItem("refresh_token",data.refresh_token);
  const current = new Date()
  const expireTime = current.setTime(current.getTime() + 1000 * data.expires_in)
  localStorage.setItem("expireTime",expireTime+"");
}

request.interceptors.response.use((response, options) => {
  //拦截返回后的特殊处理
  // if(response.data.code == 1000001){
  //   console.log(response.data.msg)
  //   //通过返回的code 提示 token 过期 、token校验失败，做相应跳转
  // }
  console.log(response)
  return response;
});

export default request;
