type DefaultSettings =  {
  authorizationValue: String;
};

const proSettings: DefaultSettings = {
    // 获取令牌时，请求头信息(Basic Base64.encode(client_id:client_secret))
  authorizationValue: 'Basic cWluZ2Zlbmc6MTIzNDU2',
};

export type { DefaultSettings };

export default proSettings;
