import request from '@/utils/request';
import settings from '@/utils/settings'

export type LoginParamsType = {
  userName: string;
  password: string;
  mobile: string;
  captcha: string;
  grant_type: string;
  refresh_token:string;
};

export async function fakeAccountLogin(params: LoginParamsType) {
  params = {...params , grant_type : 'password'}
  return request('/auth/oauth/token?'+tansParams(params), {
    method: 'POST',
    data: params,
    headers: {
      Authorization: settings.authorizationValue+"",
    },
  });
}

export async function refresh(refresh_token: string) {
  return request('/auth/oauth/token?refresh_token'+refresh_token+'&grant_type=refresh_token', {
    method: 'POST',
    headers: {
      Authorization: settings.authorizationValue+"",
    },
  });
}


function tansParams(params:LoginParamsType) {
  let result = ''
  Object.keys(params).forEach((key) => {
    if (!Object.is(params[key], undefined) && !Object.is(params[key], null)) {
      result += encodeURIComponent(key) + '=' + encodeURIComponent(params[key]) + '&'
    }
  })
  if(result.length>0){
    result = result.substring(0,result.length-1)
  }
  return result
}


// export async function getFakeCaptcha(mobile: string) {
//   return request(`/api/login/captcha?mobile=${mobile}`);
// }
